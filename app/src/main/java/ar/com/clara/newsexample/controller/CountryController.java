package ar.com.clara.newsexample.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ar.com.clara.newsexample.model.pojo.Country;

/**
 * Created by digitalhouse on 2/9/18.
 */

public class CountryController {

    public ArrayList<String> filterFavCountries(List<Country> countries){
        ArrayList<String> favCountries = new ArrayList<>();
        for (Country country: countries){
            if (country.getFavorite()){
                favCountries.add(country.getCountryName());
            }
        }
        return favCountries;
    }



}
