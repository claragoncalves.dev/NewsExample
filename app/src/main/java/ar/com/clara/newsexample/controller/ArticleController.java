package ar.com.clara.newsexample.controller;

import java.util.ArrayList;
import java.util.List;

import ar.com.clara.newsexample.model.dao.ArticleDao;
import ar.com.clara.newsexample.model.pojo.Article;
import ar.com.clara.newsexample.util.ResultListener;

/**
 * Created by digitalhouse on 1/29/18.
 */

public class ArticleController {

    public void getArticles(final ResultListener<List<Article>> listener, ArrayList<String> countries){
        ArticleDao articleDao = new ArticleDao();
        for (String country:countries) {
            articleDao.getArticles(new ResultListener<List<Article>>() {
                @Override
                public void finish(List<Article> result) {
                    listener.finish(result);
                }
            },country);
        }
    }
}
