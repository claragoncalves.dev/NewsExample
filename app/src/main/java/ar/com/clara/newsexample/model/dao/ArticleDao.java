package ar.com.clara.newsexample.model.dao;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.util.List;

import ar.com.clara.newsexample.model.pojo.Article;
import ar.com.clara.newsexample.model.pojo.ArticleContainer;
import ar.com.clara.newsexample.util.DaoException;
import ar.com.clara.newsexample.util.HTTPConnectionManager;
import ar.com.clara.newsexample.util.NewsHelper;
import ar.com.clara.newsexample.util.ResultListener;

/**
 * Created by digitalhouse on 1/29/18.
 */

public class ArticleDao {

    public void getArticles(ResultListener<List<Article>> listener, String country){
        ArticleTask articleTask = new ArticleTask(listener);
        articleTask.execute(NewsHelper.getTopheadlinesPorPais(country));
    }

    private class ArticleTask extends AsyncTask<String, Void, List<Article>>{
        private ResultListener<List<Article>> listener;

        public ArticleTask(ResultListener<List<Article>> listener) {
            this.listener = listener;
        }

        @Override
        protected List<Article> doInBackground(String... strings) {
            HTTPConnectionManager connectionManager = new HTTPConnectionManager();
            String link = strings[0];
            String input = null;

            try {
                input = connectionManager.getRequestString(link);
            } catch (DaoException e) {
                e.printStackTrace();
            }

            Gson gson = new Gson();
            ArticleContainer container = gson.fromJson(input, ArticleContainer.class);

            return container.getArticles();
        }

        @Override
        protected void onPostExecute(List<Article> articles) {
            listener.finish(articles);
        }
    }
}
