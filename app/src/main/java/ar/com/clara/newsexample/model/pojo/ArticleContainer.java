package ar.com.clara.newsexample.model.pojo;

import java.util.List;

/**
 * Created by digitalhouse on 1/29/18.
 */

public class ArticleContainer {
    private List<Article> articles;

    public List<Article> getArticles() {
        return articles;
    }
}
