package ar.com.clara.newsexample.util;

/**
 * Created by digitalhouse on 1/29/18.
 */

public class DaoException extends Exception {
    public DaoException(String error) {
        super(error);
    }
}
