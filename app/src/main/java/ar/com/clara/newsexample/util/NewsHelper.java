package ar.com.clara.newsexample.util;

/**
 * Created by digitalhouse on 1/29/18.
 */

public class NewsHelper {

    private static final String BASE_URL = "https://newsapi.org/v2/";
    private static final String API_KEY = "eec8c4938dd64617b09e1628a706a77e";

    private static final String PARAM_API_KEY = "apikey=" + API_KEY;
    private static final String PARAM_SOURCES = "sources=";
    private static final String PARAM_DOMAINS = "domains=";
    private static final String PARAM_QUERY = "q=";
    private static final String PARAM_CATEGORY = "category=";
    private static final String PARAM_COUNTRY = "country=";
    private static final String PARAM_LANGUAGES = "language=";
    private static final String PARAM_SORT_BY = "sortBy=";


    private static final String RES_FUENTES = "sources";
    private static final String RES_TOP = "top-headlines";
    private static final String RES_EVERYTHING = "everything";


    private static final String PAGE = "page=";


    //SORT BY
    public static final String sort_RELEVANCY = "relevancy";
    public static final String sort_POPULARITY = "popularity";
    public static final String sort_PUBLISHED_AT = "publishedAt";


    //IDIOMAS
    public static final String language_ARABE = "ar";
    public static final String language_INGLES = "en";
    public static final String language_CHINO = "cn";
    public static final String language_ALEMAN = "de";
    public static final String language_ESPAÑOL = "es";
    public static final String language_FRANCES = "fr";
    public static final String language_HEBREO = "he";
    public static final String language_ITALIANO = "it";
    public static final String language_PAISES_BAJOS = "nl";
    public static final String language_NORUEGO = "no";
    public static final String language_PORTUGUES = "pt";
    public static final String language_RUSO = "ru";
    public static final String language_SUECO = "sv";
    public static final String language_UD = "ud";

    //CATEGORIAS
    public static final String category_BUSINESS = "business";
    public static final String category_ENTERTAINMENT = "entertainment";
    public static final String category_GAMING = "gaming";
    public static final String category_GENERAL = "general";
    public static final String category_HEALTH_AND_MEDICAL = "health-and-medical";
    public static final String category_MUSIC = "music";
    public static final String category_POLITICS = "politics";
    public static final String category_SCIENCENATURE = "science-and-nature";
    public static final String category_SPORT = "sport";
    public static final String category_TECHNOLOGY = "technology";

    //PAISES
    public static final String country_ARGENTINA = "ar";
    public static final String country_AUSTRALIA = "au";
    public static final String country_BRASIL = "br";
    public static final String country_CANADA = "ca";
    public static final String country_CHINA = "cn";
    public static final String country_ALEMANIA = "de";
    public static final String country_ESPAÑA = "es";
    public static final String country_FRANCIA = "fr";
    public static final String country_GRANBRETANIA = "gb";
    public static final String country_HONG_KONG = "hk";
    public static final String country_IRLANDA = "ie";
    public static final String country_INDIA = "in";
    public static final String country_ISLANDIA = "is";
    public static final String country_ITALIA = "it";
    public static final String country_PAISES_BAJOS = "nl";
    public static final String country_NORUEGA = "no";
    public static final String country_PAKISTAN = "pk";
    public static final String country_RUSIA = "ru";
    public static final String country_DESCONOCIDO2 = "sa";
    public static final String country_DESCONOCIDO3 = "sv";
    public static final String country_ESTADOSUNIDOS = "us";
    public static final String country_DESCONOCIDO = "za";


    //PEDIDOS ARTICULOS TOP
    public static String getTopheadlinesPorFuente (String fuente){
        return  BASE_URL + RES_TOP + "?" + PARAM_SOURCES + fuente + "&" + PARAM_API_KEY;
    }

    public static String getTopheadlinesPorConsulta (String query){
        return  BASE_URL + RES_TOP + "?" + PARAM_QUERY + query  + "&" + PARAM_API_KEY;
    }

    public static String getTopheadlinesPorCategorias (String categoria){
        return  BASE_URL + RES_TOP + "?" + PARAM_CATEGORY + categoria + "&" + PARAM_API_KEY;
    }

    public static String getTopheadlinesPorIdioma (String idioma){
        return  BASE_URL + RES_TOP + "?" + PARAM_LANGUAGES + idioma  + "&" + PARAM_API_KEY;
    }

    public static String getTopheadlinesPorPais (String pais){
        return  BASE_URL + RES_TOP + "?" + PARAM_COUNTRY + pais + "&" + PARAM_API_KEY;
    }


    //PEDIDOS ARTICULOS EVERYTHING
    public static String getEverythingPorDominio (String dominio){
        return  BASE_URL + RES_EVERYTHING + "?" + PARAM_DOMAINS + dominio + "&" + PARAM_API_KEY;
    }

    public static String getEverythingPorFuente (String fuente){
        return  BASE_URL + RES_EVERYTHING + "?" + PARAM_SOURCES + fuente + "&" + PARAM_API_KEY;
    }

    public static String getEverythingPorConsulta (String query){
        return  BASE_URL + RES_EVERYTHING + "?" + PARAM_QUERY + query  + "&" + PARAM_SORT_BY + sort_RELEVANCY + "&" + PARAM_API_KEY;
    }

    public static String getEverythingPorConsultaNewest (String query){
        return  BASE_URL + RES_EVERYTHING + "?" + PARAM_QUERY + query  + "&" + PARAM_SORT_BY + sort_PUBLISHED_AT + "&" + PARAM_API_KEY;
    }

    public static String getEverythingPorConsultaPopular (String query){
        return  BASE_URL + RES_EVERYTHING + "?" + PARAM_QUERY + query  + "&" + PARAM_SORT_BY + sort_POPULARITY + "&" + PARAM_API_KEY;
    }



    //PEDIDOS FUENTES
    public static String getFuentes(){
        return BASE_URL + RES_FUENTES + "?" + PARAM_API_KEY;
    }

    public static String getFuentesPorCategoria (String categoria){
        return BASE_URL + RES_FUENTES + "?" + PARAM_CATEGORY + categoria + "&" + PARAM_API_KEY;
    }

    public static String getFuentesPorIdioma(String idioma){
        return BASE_URL + RES_FUENTES + "?" + PARAM_LANGUAGES + idioma + "&" + PARAM_API_KEY;
    }

    public static String getFuentesPorPais(String pais){
        return BASE_URL + RES_FUENTES + "?" + PARAM_COUNTRY + pais + "&" + PARAM_API_KEY;
    }

}

