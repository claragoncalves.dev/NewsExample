package ar.com.clara.newsexample.view.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ar.com.clara.newsexample.R;
import ar.com.clara.newsexample.model.pojo.Country;

/**
 * Created by digitalhouse on 2/9/18.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {
    private List<Country> countries;

    public CountryAdapter(List<Country> countries) {
        this.countries = countries;
    }

    public List<Country> getCountries() {
        return countries;
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CountryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.country_cell,parent, false));
    }

    @Override
    public void onBindViewHolder(final CountryViewHolder holder, final int position) {
        holder.bindCountry(countries.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.setSelected(countries.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public class CountryViewHolder extends RecyclerView.ViewHolder{
        private TextView textViewCountryName;

        public CountryViewHolder(View itemView) {
            super(itemView);
            textViewCountryName = itemView.findViewById(R.id.textviewCountryName);
        }

        public void bindCountry(Country country){
            if (country.getFavorite()){
                textViewCountryName.setBackgroundColor(Color.GREEN);
            }else {
                textViewCountryName.setBackgroundColor(Color.GRAY);
            }
            textViewCountryName.setText(country.getCountryName());
        }

        public void setSelected(Country country){
            if (country.getFavorite()){
                textViewCountryName.setBackgroundColor(Color.GRAY);
                country.setFavorite(false);
            }else {
                textViewCountryName.setBackgroundColor(Color.GREEN);
                country.setFavorite(true);
            }
        }
    }
}
