package ar.com.clara.newsexample.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import ar.com.clara.newsexample.R;
import ar.com.clara.newsexample.model.pojo.Country;
import ar.com.clara.newsexample.util.NewsHelper;
import ar.com.clara.newsexample.view.adapters.CountryAdapter;


public class OptionsFragment extends Fragment {

    private CountryAdapter countryAdapter;
    private CountrySelector countrySelector;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_options, container, false);
        countryAdapter = new CountryAdapter(loadCountries());
        RecyclerView recyclerViewCountries = view.findViewById(R.id.recyclerviewCountries);
        recyclerViewCountries.setLayoutManager(new GridLayoutManager(getContext(),3));
        recyclerViewCountries.setHasFixedSize(true);
        recyclerViewCountries.setAdapter(countryAdapter);
        Button buttonFavsSelected = view.findViewById(R.id.buttonFavsSelected);
        buttonFavsSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countrySelector = (CountrySelector) getContext();
                countrySelector.selectedCountries(countryAdapter.getCountries());
            }
        });
        return view;
    }

    private List<Country> loadCountries(){
        List<Country> countries = new ArrayList<>();
        countries.add(new Country(NewsHelper.country_ALEMANIA, false));
        countries.add(new Country(NewsHelper.country_ARGENTINA,false));
        countries.add(new Country(NewsHelper.country_AUSTRALIA, false));
        countries.add(new Country(NewsHelper.country_BRASIL, false));
        countries.add(new Country(NewsHelper.country_CANADA, false));
        countries.add(new Country(NewsHelper.country_CHINA, false));
        countries.add(new Country(NewsHelper.country_ESPAÑA, false));
        countries.add(new Country(NewsHelper.country_ESTADOSUNIDOS, false));
        countries.add(new Country(NewsHelper.country_FRANCIA, false));
        countries.add(new Country(NewsHelper.country_GRANBRETANIA, false));
        countries.add(new Country(NewsHelper.country_HONG_KONG, false));
        countries.add(new Country(NewsHelper.country_INDIA, false));
        countries.add(new Country(NewsHelper.country_IRLANDA, false));
        countries.add(new Country(NewsHelper.country_ISLANDIA, false));
        countries.add(new Country(NewsHelper.country_ITALIA, false));
        countries.add(new Country(NewsHelper.country_NORUEGA, false));
        countries.add(new Country(NewsHelper.country_PAISES_BAJOS, false));
        countries.add(new Country(NewsHelper.country_PAKISTAN, false));
        countries.add(new Country(NewsHelper.country_RUSIA, false));
        return countries;
    }

    public interface CountrySelector{
        public void selectedCountries(List<Country> countries);
    }
}
