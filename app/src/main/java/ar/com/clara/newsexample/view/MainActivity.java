package ar.com.clara.newsexample.view;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import ar.com.clara.newsexample.R;
import ar.com.clara.newsexample.controller.CountryController;
import ar.com.clara.newsexample.model.pojo.Article;
import ar.com.clara.newsexample.model.pojo.Country;
import ar.com.clara.newsexample.view.adapters.ArticleAdapter;

public class MainActivity extends AppCompatActivity implements ArticleAdapter.ArticleSelected, OptionsFragment.CountrySelector{

    private ArticleListFragment articleListFragment;
    private OptionsFragment optionsFragment;
    private CountryController countryController;
    private static final String LIST_FRAGMENT = "listFragment";
    private static final String DETAIL_FRAGMENT = "detailFragment";
    private static final String OPTIONS_FRAGMENT = "optionsFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        countryController = new CountryController();
        articleListFragment = new ArticleListFragment();
        optionsFragment = new OptionsFragment();
        placeFragment(optionsFragment,OPTIONS_FRAGMENT);
    }

    private void placeFragment(Fragment fragment, String id){
        if (id.equals(LIST_FRAGMENT) || id.equals(OPTIONS_FRAGMENT)){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolderMain, fragment, id).commit();
        }else{
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolderMain, fragment, id).addToBackStack(null).commit();
        }
    }


    @Override
    public void onArticleSelected(Article article) {
        ArticleDetailFragment articleDetailFragment = new ArticleDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ArticleDetailFragment.KEY_ARTICLE,article);
        articleDetailFragment.setArguments(bundle);
        placeFragment(articleDetailFragment,DETAIL_FRAGMENT);
    }

    @Override
    public void selectedCountries(List<Country> countries) {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(ArticleListFragment.FAV_COUNTRIES, countryController.filterFavCountries(countries));
        articleListFragment.setArguments(bundle);
        placeFragment(articleListFragment, LIST_FRAGMENT);
    }
}
