package ar.com.clara.newsexample.util;

/**
 * Created by digitalhouse on 1/29/18.
 */

public interface ResultListener<T> {
    void finish(T result);
}
