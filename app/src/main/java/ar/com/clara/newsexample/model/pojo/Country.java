package ar.com.clara.newsexample.model.pojo;

/**
 * Created by digitalhouse on 2/9/18.
 */

public class Country {
    private String countryName;
    private Boolean isFavorite;

    public Country(String countryName, Boolean isFavorite) {
        this.countryName = countryName;
        this.isFavorite = isFavorite;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }
}
