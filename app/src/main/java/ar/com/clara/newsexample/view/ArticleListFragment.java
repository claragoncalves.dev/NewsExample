package ar.com.clara.newsexample.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ar.com.clara.newsexample.R;
import ar.com.clara.newsexample.controller.ArticleController;
import ar.com.clara.newsexample.model.pojo.Article;
import ar.com.clara.newsexample.util.ResultListener;
import ar.com.clara.newsexample.view.adapters.ArticleAdapter;


public class ArticleListFragment extends Fragment {

    public static final String FAV_COUNTRIES = "favCountries";
    private ArticleAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerviewArticles);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (3 - position % 3);
            }
        });
        adapter = new ArticleAdapter();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        getArticles();
        return view;
    }

    private void getArticles(){
        Bundle bundle = getArguments();
        ArrayList<String> favCountries = bundle.getStringArrayList(FAV_COUNTRIES);
        ArticleController articleController = new ArticleController();
        articleController.getArticles(new ResultListener<List<Article>>() {
            @Override
            public void finish(List<Article> result) {
                adapter.addArticles(result);
            }
        },favCountries);
    }

}
