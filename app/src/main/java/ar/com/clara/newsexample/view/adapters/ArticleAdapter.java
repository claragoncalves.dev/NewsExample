package ar.com.clara.newsexample.view.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import java.util.ArrayList;
import java.util.List;
import ar.com.clara.newsexample.R;
import ar.com.clara.newsexample.model.pojo.Article;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by digitalhouse on 1/29/18.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>{
    private List<Article> articles;
    private ArticleSelected articleSelected;

    public ArticleAdapter() {
        this.articles = new ArrayList<>();
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
        notifyDataSetChanged();
    }

    public void addArticles(List<Article> articles){
        this.articles.addAll(articles);
        notifyDataSetChanged();
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ArticleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.article_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(final ArticleViewHolder holder, final int position) {
        holder.bindArticle(articles.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                articleSelected = (ArticleSelected) view.getContext();
                articleSelected.onArticleSelected(articles.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder{
        TextView textViewTitle;
        ImageView imageViewArticle;
        RequestOptions requestOptions;
        public ArticleViewHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textviewArticleCellTitle);
            imageViewArticle = itemView.findViewById(R.id.imageviewArticleImage);
            requestOptions = new RequestOptions().error(R.drawable.placeholder).placeholder(R.drawable.placeholder);
        }

        public void bindArticle(Article article){
            textViewTitle.setText(article.getTitle());
            Glide.with(itemView.getContext()).load(article.getUrlToImage()).apply(requestOptions).transition(withCrossFade()).into(imageViewArticle);
        }
    }

    public interface ArticleSelected{
        void onArticleSelected(Article article);
    }
}
